#!/bin/bash
var=$(git tag -l --points-at HEAD) && ([ -z "$var" ]  && (version=$(git describe --abbrev=0 ${GIT_TAGS_PARAM}) && version=${version:1} && a=( ${version//./ } ) && (( a[2] = a[2] + 1 )) && next_version="${a[0]}.${a[1]}.${a[2]}" && echo $next_version && git tag "v$next_version") || (echo "Is not empty"))
git push https://gitlab.com/tamas.kapitany/tagging-test.git --tags --no-verify
